import 'package:drift/drift.dart';
import 'package:to_do_list_app/database/to_do_list_database.dart';
import 'package:to_do_list_app/database/to_do_list_table.dart';
part 'to_do_list_dao.g.dart';

@DriftAccessor(tables: [ToDoListTable])
class ToDoListDao extends DatabaseAccessor<ToDoListDatabase> with _$ToDoListDaoMixin{
  ToDoListDao(super.toDoListDatabase);

Future<int> insertTasks(ToDoListTableCompanion toDoList) async{
  // print(toDoList);
  return await into(toDoListTable).insert(toDoList);

}

Stream<List<ToDoList>> getAllTasks() {
  return select(toDoListTable)
  .watch();
}

Stream<List<ToDoList>> getToDoList() {
  return (select(toDoListTable)
  ..where((toDoListTable) => toDoListTable.complete.equals(false)))
  .watch();
}

Stream<List<ToDoList>> getCompletedTasks() {
  return (select(toDoListTable)
  ..where((toDoListTable) => toDoListTable.complete.equals(true)))
  .watch();
}

Future<int> updateComplete({required int id, required bool complete}) async{
  // print(complete);
  return await (update(toDoListTable)
  ..where((tbl) => tbl.id.equals(id)))
  .write(ToDoListTableCompanion(complete: Value(complete)));
}

Future<bool> updateTask(ToDoListTableCompanion toDoList) async{
  return await update(toDoListTable).replace(toDoList);
}

Future<int> deleteTask(ToDoList toDoList) async{
  return await delete(toDoListTable).
  delete(toDoList);
}

}