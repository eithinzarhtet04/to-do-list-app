import 'package:drift/drift.dart';
import 'package:flutter/material.dart';
import 'package:to_do_list_app/database/to_do_list_database.dart';
import 'package:to_do_list_app/main.dart';
import 'package:to_do_list_app/ui/my_tasks.dart';
import 'package:to_do_list_app/ui/new_list.dart';
import 'package:to_do_list_app/ui/to_do_list.dart';
import 'package:to_do_list_app/ui/to_do_list_detail.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  ToDoListDatabase _toDoListDatabase = ToDoListDatabase();

  @override
  void initState() {
    super.initState();
    // birthday =  
  }
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Tasks'),
          centerTitle: true,
          bottom: const TabBar(
            tabs: [
              Tab(text: 'My Tasks',),
              Tab(text: 'Todo',),
              Tab(icon: Icon(Icons.add), text: 'New list',)
            ]
          )
        ),
        body: TabBarView(
          children: [
            MyTasks(),
            ToDoListScreen(),
            NewList(),
          ]
        ),
      ),
    );
  }
}