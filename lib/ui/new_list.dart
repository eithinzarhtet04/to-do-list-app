import 'package:drift/drift.dart';
import 'package:flutter/material.dart';
import 'package:to_do_list_app/database/to_do_list_database.dart';
import 'package:to_do_list_app/main.dart';

class NewList extends StatefulWidget {

  NewList({super.key});

  @override
  State<NewList> createState() => _NewListState();
}

class _NewListState extends State<NewList> {
  GlobalKey<FormState> _key = GlobalKey();

  String? title;

  DateTime? datetime;
  String? _today;

  bool? complete;

  ToDoListDatabase _toDoListDatabase = ToDoListDatabase();

  // final day = DateTime.now().day;
  // final month = DateTime.now().month;
  // final year = DateTime.now().year;

  @override
  void initState() {
    super.initState();
    // _today = day.toString() + month.toString() + year.toString();
  }
  Widget build(BuildContext context) {
    return Padding(
              padding: const EdgeInsets.all(10.0),
              child: Form(
                key: _key,
                child: ListView(
                  children: [
                    const Center(child: Text('Create New Task', style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500),)),
                    const SizedBox(
                      height: 30,
                    ),
                    TextFormField(
                      validator: (str){
                        if(str == null || str.isEmpty){
                          return 'Please enter the name of task';
                        }
                      },
                      onSaved: (str){
                        title = str;
                      },
                      decoration: const InputDecoration(
                        hintText: 'Enter title',
                        border: OutlineInputBorder()
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    TextButton.icon(
                      onPressed: () async{
                          datetime = await showDatePicker(
                            context: context, 
                            initialDate: DateTime(1995), 
                            firstDate: DateTime(1990), 
                            lastDate: DateTime.now()
                          );
                          setState(() {
                            
                          });   
                      }, 
                      icon: Icon(Icons.calendar_month), 
                      label: (datetime == null) ? Text('Today') : Text('${datetime!.day} ${datetime!.month} ${datetime!.year}')  // Text(DateTime.now().day.toString()+ '' + DateTime.now().month.toString())
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    ElevatedButton.icon(
                      onPressed: (){
                        if(datetime == null){
                          ScaffoldMessenger.of(context)
                          .showSnackBar(const SnackBar(content: Text('Please select the datetime')));
                        }
                        if(_key.currentState != null && _key.currentState!.validate()){
                          _key.currentState!.save();
                          _toDoListDatabase.toDoListDao.insertTasks(
                            ToDoListTableCompanion(
                              title: Value(title!),
                              datetime: Value(datetime!),
                              complete: const Value(false)
                            )
                          );
                        }
                        Navigator.push(context, MaterialPageRoute(builder: (_) => MyApp()));
                      }, 
                      icon: const Icon(Icons.save), 
                      label: const Text('Save')
                    ),
                  ],
                )
              ),
            );
  }
}