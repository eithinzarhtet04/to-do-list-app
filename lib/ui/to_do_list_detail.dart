import 'package:flutter/material.dart';
import 'package:to_do_list_app/database/to_do_list_database.dart';
class ToDoListDetail extends StatelessWidget {
  ToDoList data;
  ToDoListDetail({super.key, required this.data});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(data.title),
      ),
      body: Center(
        child: Column(
          children: [
            ListTile(title : Text(data.title)),
            ListTile(title: Text(data.datetime.toString()),),
            ListTile(title: (data.complete == true) ? CircleAvatar(backgroundColor: Colors.green[50], maxRadius:30, minRadius: 1, child: Text('Done', style: TextStyle(color: Colors.blue, fontSize: 16),)) : Text(''),)
          ],
        ),
      )
    );
  }
}