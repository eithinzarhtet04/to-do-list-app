import 'package:flutter/material.dart';
import 'package:to_do_list_app/database/to_do_list_database.dart';
import 'package:to_do_list_app/ui/to_do_list_detail.dart';

class MyTasks extends StatefulWidget {
  MyTasks({super.key});

  @override
  State<MyTasks> createState() => _MyTasksState();
}

class _MyTasksState extends State<MyTasks> {
  ToDoListDatabase _toDoListDatabase = ToDoListDatabase();
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<ToDoList>>(
                stream: _toDoListDatabase.toDoListDao.getCompletedTasks(), 
                builder: ((context, snapshot) {
                  if(snapshot.hasData){
                    return ListView.builder(
                    itemCount: snapshot.data!.length,
                    itemBuilder: ((context, index) {
                      // print(snapshot.data);
                      return Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (_) => ToDoListDetail(data: snapshot.data![index]),));
                          },
                          child: Card(
                            color: Colors.blue[50],
                            elevation: 3,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: ListTile(
                              title: Text(snapshot.data![index].title.toString(), style: TextStyle(decoration: (snapshot.data![index].complete == true) ? TextDecoration.lineThrough : TextDecoration.none)),
                              leading: (
                                snapshot.data![index].complete == true) ? 
                                IconButton(onPressed: (){ 
                                print('Done'); 
                                setState(() {
                                  _toDoListDatabase.toDoListDao.updateComplete(
                                   id: snapshot.data![index].id, complete: false
                                  );
                                });
                                }, icon: Icon(Icons.check_box_outlined)) : 
                                IconButton(onPressed: (){ 
                                  print('Pending'); 
                                  setState(() {
                                    _toDoListDatabase.toDoListDao.updateComplete(
                                      id: snapshot.data![index].id, complete: false                                  );
                                  });
                                }, icon: Icon(Icons.check_box_outline_blank)
                              ) ,
                              subtitle: Text(snapshot.data![index].datetime.toString(), style: TextStyle(decoration: (snapshot.data![index].complete == true) ? TextDecoration.lineThrough : TextDecoration.none),),
                              ),
                            ),
                          ),
                        ),
                      );
                    })
                  );
                  }
                  else if(snapshot.hasError){
                    return Center(child: Text(snapshot.hasError.toString()));
                  }
                  else{
                    return const Center(child: CircularProgressIndicator());
                  }
                  // Text(snapshot.data![index].title.toString());
                })
              );
  }
}