// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'to_do_list_database.dart';

// ignore_for_file: type=lint
class $ToDoListTableTable extends ToDoListTable
    with TableInfo<$ToDoListTableTable, ToDoList> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $ToDoListTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _titleMeta = const VerificationMeta('title');
  @override
  late final GeneratedColumn<String> title = GeneratedColumn<String>(
      'title', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _datetimeMeta =
      const VerificationMeta('datetime');
  @override
  late final GeneratedColumn<DateTime> datetime = GeneratedColumn<DateTime>(
      'datetime', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _completeMeta =
      const VerificationMeta('complete');
  @override
  late final GeneratedColumn<bool> complete = GeneratedColumn<bool>(
      'complete', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("complete" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns => [id, title, datetime, complete];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'to_do_list_table';
  @override
  VerificationContext validateIntegrity(Insertable<ToDoList> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('title')) {
      context.handle(
          _titleMeta, title.isAcceptableOrUnknown(data['title']!, _titleMeta));
    } else if (isInserting) {
      context.missing(_titleMeta);
    }
    if (data.containsKey('datetime')) {
      context.handle(_datetimeMeta,
          datetime.isAcceptableOrUnknown(data['datetime']!, _datetimeMeta));
    } else if (isInserting) {
      context.missing(_datetimeMeta);
    }
    if (data.containsKey('complete')) {
      context.handle(_completeMeta,
          complete.isAcceptableOrUnknown(data['complete']!, _completeMeta));
    } else if (isInserting) {
      context.missing(_completeMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  ToDoList map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return ToDoList(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      title: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}title'])!,
      datetime: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}datetime'])!,
      complete: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}complete'])!,
    );
  }

  @override
  $ToDoListTableTable createAlias(String alias) {
    return $ToDoListTableTable(attachedDatabase, alias);
  }
}

class ToDoList extends DataClass implements Insertable<ToDoList> {
  final int id;
  final String title;
  final DateTime datetime;
  final bool complete;
  const ToDoList(
      {required this.id,
      required this.title,
      required this.datetime,
      required this.complete});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['title'] = Variable<String>(title);
    map['datetime'] = Variable<DateTime>(datetime);
    map['complete'] = Variable<bool>(complete);
    return map;
  }

  ToDoListTableCompanion toCompanion(bool nullToAbsent) {
    return ToDoListTableCompanion(
      id: Value(id),
      title: Value(title),
      datetime: Value(datetime),
      complete: Value(complete),
    );
  }

  factory ToDoList.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return ToDoList(
      id: serializer.fromJson<int>(json['id']),
      title: serializer.fromJson<String>(json['title']),
      datetime: serializer.fromJson<DateTime>(json['datetime']),
      complete: serializer.fromJson<bool>(json['complete']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'title': serializer.toJson<String>(title),
      'datetime': serializer.toJson<DateTime>(datetime),
      'complete': serializer.toJson<bool>(complete),
    };
  }

  ToDoList copyWith(
          {int? id, String? title, DateTime? datetime, bool? complete}) =>
      ToDoList(
        id: id ?? this.id,
        title: title ?? this.title,
        datetime: datetime ?? this.datetime,
        complete: complete ?? this.complete,
      );
  @override
  String toString() {
    return (StringBuffer('ToDoList(')
          ..write('id: $id, ')
          ..write('title: $title, ')
          ..write('datetime: $datetime, ')
          ..write('complete: $complete')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, title, datetime, complete);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ToDoList &&
          other.id == this.id &&
          other.title == this.title &&
          other.datetime == this.datetime &&
          other.complete == this.complete);
}

class ToDoListTableCompanion extends UpdateCompanion<ToDoList> {
  final Value<int> id;
  final Value<String> title;
  final Value<DateTime> datetime;
  final Value<bool> complete;
  const ToDoListTableCompanion({
    this.id = const Value.absent(),
    this.title = const Value.absent(),
    this.datetime = const Value.absent(),
    this.complete = const Value.absent(),
  });
  ToDoListTableCompanion.insert({
    this.id = const Value.absent(),
    required String title,
    required DateTime datetime,
    required bool complete,
  })  : title = Value(title),
        datetime = Value(datetime),
        complete = Value(complete);
  static Insertable<ToDoList> custom({
    Expression<int>? id,
    Expression<String>? title,
    Expression<DateTime>? datetime,
    Expression<bool>? complete,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (title != null) 'title': title,
      if (datetime != null) 'datetime': datetime,
      if (complete != null) 'complete': complete,
    });
  }

  ToDoListTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? title,
      Value<DateTime>? datetime,
      Value<bool>? complete}) {
    return ToDoListTableCompanion(
      id: id ?? this.id,
      title: title ?? this.title,
      datetime: datetime ?? this.datetime,
      complete: complete ?? this.complete,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (title.present) {
      map['title'] = Variable<String>(title.value);
    }
    if (datetime.present) {
      map['datetime'] = Variable<DateTime>(datetime.value);
    }
    if (complete.present) {
      map['complete'] = Variable<bool>(complete.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ToDoListTableCompanion(')
          ..write('id: $id, ')
          ..write('title: $title, ')
          ..write('datetime: $datetime, ')
          ..write('complete: $complete')
          ..write(')'))
        .toString();
  }
}

abstract class _$ToDoListDatabase extends GeneratedDatabase {
  _$ToDoListDatabase(QueryExecutor e) : super(e);
  late final $ToDoListTableTable toDoListTable = $ToDoListTableTable(this);
  late final ToDoListDao toDoListDao = ToDoListDao(this as ToDoListDatabase);
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [toDoListTable];
}
