import 'dart:io';

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:to_do_list_app/database/to_do_list_table.dart';
import 'package:to_do_list_app/modal/to_do_list_dao.dart';
part 'to_do_list_database.g.dart';

@DriftDatabase(tables: [ToDoListTable], daos: [ToDoListDao])
class ToDoListDatabase extends _$ToDoListDatabase{
  ToDoListDatabase() : super(_createDatabase()); // to update create db function

  @override
  int get schemaVersion => 1;

}

LazyDatabase _createDatabase(){
  return LazyDatabase(() async{
    final dbPath = await getApplicationDocumentsDirectory();
    final dbFile = File(join(dbPath.path, 'todolist.db'));
    return NativeDatabase(dbFile);
  });
}