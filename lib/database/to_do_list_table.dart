import 'package:drift/drift.dart';

@DataClassName('ToDoList')
class ToDoListTable extends Table{
  IntColumn get id => integer().autoIncrement()();
  TextColumn get title => text().named('title')(); //.withLength(min: 5, max: 50)
  DateTimeColumn get datetime => dateTime().named('datetime')();
  BoolColumn get complete => boolean().named('complete')();
}